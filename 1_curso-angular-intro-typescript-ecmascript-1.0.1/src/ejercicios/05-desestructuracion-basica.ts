/*
    ===== Código de TypeScript =====
*/

console.log('Hola Mundo!');

interface Reproductor{
    volumen:number;
    segundo:number;
    cancion:string;
    detalles:Detalles;
}

interface Detalles{
    autor: string;
    anio: number;
}

const reproductor: Reproductor = {
    volumen: 90,
    segundo: 36,
    cancion: "california",
    detalles: {
        autor: "tu papá",
        anio: 1990
    } 
}

/* desestructuracion de objetos */
//const { volumen, segundo, detalles:{autor} } = reproductor;
//const { volumen: vol, segundo, detalles:{autor} } = reproductor;
const { volumen, segundo, detalles } = reproductor;
const { autor } = detalles

console.log("El volumen actual es: ", volumen);
console.log("El segundo actual es: ", segundo);
console.log("La cancion actual es: ", reproductor.cancion);
console.log("El autor es: ", detalles.autor);
console.log("El autor es: ", autor);


/*desestructuracion de arreglos*/
const dbz: string[] = ["Goku","Vegeta","Trunks"]
//const [ p1, p2, p3 ] = dbz;
const [ , p2, p3 ] = dbz;

console.log("Personaje 1: ", dbz[0]);
console.log("Personaje 1: ", p2);
console.log("Personaje 1: ", p3);

