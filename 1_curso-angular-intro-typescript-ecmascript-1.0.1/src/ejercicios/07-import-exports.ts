/*
    ===== Código de TypeScript =====
*/

console.log('Hola Mundo!');

import { Producto, calculaIVA } from "./06-desestructuracion-funcion";

const carritoCompras: Producto[] = [
    {
        desc:"moto g5",
        precio: 110
    },
    {
        desc:"oppo",
        precio: 180
    },
];

const [total, iva] = calculaIVA(carritoCompras);
console.log("Total",total);
console.log("IVA",iva);
