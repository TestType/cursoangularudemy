/*
    ===== Código de TypeScript =====
*/

console.log('Hola Mundo!');

let habilidades1: (string | number)[]= ["bash", "counter", "healing", 100];

let habilidades2: string[]= ["bash", "counter", "healing"];

interface Personaje{
    nombre: string;
    hp:number;
    habilidades: string[];
    puebloNatal?: string;
}

const personaje: Personaje = {
    nombre: "strider",
    hp: 100,
    habilidades : ["bash", "counter", "healing"]
}

personaje.puebloNatal = "Pueblo paleta"

console.table(personaje);


