/*
    ===== Código de TypeScript =====
*/

console.log('Hola Mundo!');

let nombre:string = "Tamino";
let hp1: number = 1235;
let hp2: number | string = "Full";
let estado: boolean = true;

console.log(nombre);
console.log(hp1, hp2);
console.log(estado);

