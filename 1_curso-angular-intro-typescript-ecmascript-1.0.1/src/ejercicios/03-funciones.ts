/*
    ===== Código de TypeScript =====
*/

console.log('Hola Mundo!');

function sumar(a:number, b:number):number{
    return a + b;
}

const sumarFlecha = (a:number, b:number):number =>{
    return a+b;
}

function multiplicar(numero:number, otroNumero?:number, base:number = 2){
    return numero * base;
}

interface PersonajeLOR{
    nombre: string;
    pv: number;
    mostrarHp:() => void;
}

function curar(personaje:PersonajeLOR, curarX: number):void{
    personaje.pv += curarX;
    console.log(personaje) 
}

const nuevoPersonaje: PersonajeLOR = {
    nombre: "Pamina",
    pv: 50,
    mostrarHp(){
        console.log("Puntos de vida: ", this.pv);
    }
}

curar(nuevoPersonaje, 20);
nuevoPersonaje.mostrarHp();

const resultado = sumar(1,5);
console.log(resultado);
console.log(sumarFlecha(5,9));
console.log(multiplicar(5,0,10));